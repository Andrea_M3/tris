#include "../../vsgl2.h"
#include <sstream>

using namespace vsgl2;

const int WIDTH = 735;
const int HEIGHT = 735;
const string TITLE = "Tris";
const Color BLACK = Color(0, 0, 0, 255);

// ----------RESOURCES-----------
string FONT = "vt323.ttf";
string CIRCLE = "circle.png";
string CROSS = "cross.png";
//-------------------------------

const int GRID_SIZE = 3;
const int RECT_SIZE = 245;
bool canReset = false;

class PLAYER {
    int PLAYER_NUMBER;
    Color PLAYER_COLOR;
    bool winner;
    int PLAYER_SCORE;
    bool addedScore;
public:

    PLAYER(int id, Color color, int score) {
        PLAYER_NUMBER = id;
        PLAYER_COLOR = color;
        winner = false;
        PLAYER_SCORE = score;
        addedScore = false;
    }

    int getID() {
        return PLAYER_NUMBER;
    }

    int getScore() {
        return PLAYER_SCORE;
    }

    void setScore(int score) {
        PLAYER_SCORE = score;
    }

    void addScore() {
        if(!addedScore){
            PLAYER_SCORE++;
            addedScore = true;
        }
    }

    Color getColor() {
        return PLAYER_COLOR;
    }

    void setWinner(bool state) {
        winner = state;
    }

    bool isWinner() {
        return winner;
    }

    void canAddScore() {
        addedScore = false;
    }
};

const int EMPTY = 0;
PLAYER player1 = PLAYER(1, Color(255, 0, 0, 255), 0);
PLAYER player2 = PLAYER(2, Color(0, 0, 255, 255), 0);

int active_player = player1.getID();
ostringstream win_string;

void init_window(int m[][GRID_SIZE]);
void render(int m[][GRID_SIZE]);
void renderScore();
void update_logic(int m[][GRID_SIZE]);
void invert_active_player();
void verify_win(int m[][GRID_SIZE]);
void setWinner(int player, int m[][GRID_SIZE]);
void press_right_mouse_button_to_reset(int m[][GRID_SIZE]);
void reset(int m[][GRID_SIZE]);

int main(int argc, char* argv[]) {

    int play[GRID_SIZE][GRID_SIZE];
    init_window(play);

    while(!done()) {
        update_logic(play);
        render(play);
        update();
    }
    close();
    return 0;
}

void render(int m[][GRID_SIZE]){
    for(int x = 0; x < GRID_SIZE; x++){
        for(int y = 0; y < GRID_SIZE; y++){
            if(m[x][y] == EMPTY) {
                draw_filled_rect(x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE, BLACK);
            } else if (m[x][y] == player1.getID()){
                draw_filled_rect(x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE, BLACK);
                draw_image(CROSS, x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE, 255);
            } else if (m[x][y] == player2.getID()){
                draw_filled_rect(x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE, BLACK);
                draw_image(CIRCLE, x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE, 255);
            }
        }
    }

    renderScore();

    if(player1.isWinner() || player2.isWinner()) {
        win_string << "Player ";
        if(player1.isWinner())
            win_string << player1.getID();
        else
            win_string << player2.getID();
        win_string << " win!";
        draw_text(FONT, 50, win_string.str(), WIDTH / 2 - text_width(FONT, 50, win_string.str()) / 2, HEIGHT / 2 - text_height(FONT, 50, win_string.str()), Color(125,0,255,255));
    } else {
        int filled = 0;
        for(int x = 0; x < GRID_SIZE; x++)
            for(int y = 0; y < GRID_SIZE; y++)
                if(m[x][y] != EMPTY)
                    filled++;
        if(filled >= 9){
            win_string << "Draw...";
            draw_text(FONT, 50, win_string.str(), WIDTH / 2 - text_width(FONT, 50, win_string.str()) / 2, HEIGHT / 2 - text_height(FONT, 50, win_string.str()), Color(125,0,255,255));
        }
    }
    win_string.str("");
}

ostringstream player1score;
ostringstream player2score;
void renderScore() {
    player1score << "Player1 : " << player1.getScore();
    player2score << "Player2 : " << player2.getScore();
    draw_text(FONT, 26, player1score.str(), 4, 0, Color(0, 0, 255, 255));
    draw_text(FONT, 26, player2score.str(), 4, text_height(FONT, 26, player1score.str()), Color(0, 0, 255, 255));
    player1score.str("");
    player2score.str("");
}

void update_logic(int m[][GRID_SIZE]) {
    if(!player1.isWinner() && !player2.isWinner())
        if(mouse_left_button_pressed()) {
            int mouse_X = get_mouse_x() / RECT_SIZE;
            int mouse_Y = get_mouse_y() / RECT_SIZE;
            if(m[mouse_X][mouse_Y] == EMPTY && !canReset) {
                m[mouse_X][mouse_Y] = active_player;
                invert_active_player();
            }
        }
    verify_win(m);
}

void verify_win(int m[][GRID_SIZE]){
    int filled = 1;
    for(int x = 0; x < GRID_SIZE; x++) {
        for(int y = 0; y < GRID_SIZE; y++) {
            if(m[x][y] != EMPTY) {
                filled++;
            }
        }
    }

    if(m[0][0] == m[0][1] && m[0][1] == m[0][2])
        setWinner(m[0][0], m);
    else if(m[1][0] == m[1][1] && m[1][1] == m[1][2])
        setWinner(m[1][0], m);
    else if(m[2][0] == m[2][1] && m[2][1] == m[2][2])
        setWinner(m[2][0], m);
    else if(m[0][0] == m[1][0] && m[1][0] == m[2][0])
        setWinner(m[0][0], m);
    else if(m[0][1] == m[1][1] && m[1][1] == m[2][1])
        setWinner(m[0][1], m);
    else if(m[0][2] == m[1][2] && m[1][2] == m[2][2])
        setWinner(m[0][2], m);
    else if(m[0][0] == m[1][1] && m[1][1] == m[2][2])
        setWinner(m[0][0], m);
    else if(m[0][2] == m[1][1] && m[1][1] == m[2][0])
        setWinner(m[0][2], m);

    if(filled >= 9) {
        setWinner(0, m);
    }
}



void setWinner(int player, int m[][GRID_SIZE]) {
    if(player1.getID() == player) {
        player1.setWinner(true);
        player1.addScore();
        canReset = true;
    } else if(player2.getID() == player) {
        player2.setWinner(true);
        player2.addScore();
        canReset = true;
    } else if(player == EMPTY) {
        canReset = true;
    }
    press_right_mouse_button_to_reset(m);
}

void invert_active_player() {
    if(active_player == player1.getID())
        active_player = player2.getID();
    else
        active_player = player1.getID();
}

void init_window(int m[][GRID_SIZE]) {
    init();
    set_window(WIDTH, HEIGHT, TITLE);

    for(int x = 0; x < GRID_SIZE; x++)
        for(int y = 0; y < GRID_SIZE; y++)
            m[x][y] = EMPTY;
}
void press_right_mouse_button_to_reset(int m[][GRID_SIZE]) {
    if(mouse_right_button_pressed())
        if(canReset)
            reset(m);
    canReset = false;
}

void reset(int m[][GRID_SIZE]){
    for(int x = 0; x < GRID_SIZE; x++)
        for(int y = 0; y < GRID_SIZE; y++)
            m[x][y] = EMPTY;
    player1.setWinner(false);
    player2.setWinner(false);
    win_string.str("");
    active_player = player1.getID();
    player1.canAddScore();
    player2.canAddScore();
}


/*// TODO
class Button {
    int x, y;
    int width, height;
    Color color;
public:
    void init_button(int x, int y, int width, int height, Color color){}

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }
};*/
